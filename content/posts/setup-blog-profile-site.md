---
title: "Setup my blog & profile site"
date: 2022-01-16T09:09:36+05:30
tags: ["Hugo", "Gitlab", "Static site"]
author: ["Me"]
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
summary: "Setup My profile & blog site using Hugo and gitlab"
description: "Setup My profile & blog site using Hugo and gitlab"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
---

### Installing Hugo 
[Hugo](https://gohugo.io/) is a static site generator written in go. We can install hugo easily using any of the package manager available for the different platforms. Since I am using macOS the below commmand worked for me.
We can get the detailed isntallation steps from the hugo [quickstart](https://gohugo.io/getting-started/quick-start/) guide.

```sh
brew install hugo
```

### Creating a new Hugo Site
Created a new repository which will act as the source repository for the blog site. And use the `new` hugo command to generate a new site.
```sh
hugo new site <SITE NAME>
```

Cloning the new repository, and creating a new hugo site in the cloned repository `--force` option is used to force the site creation when the directory is not empty. `-y yml` option is used to create the hugo repository using the **config.yml** instead of **config.toml**
```sh
hugo new site . --force -f yml
```

### Updating the default archetypes

### Add a theme for the Hugo site

### Update the config file
